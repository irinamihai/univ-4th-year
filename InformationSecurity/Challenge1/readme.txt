IMPORTANT! Este necesar ca rularea sa se faca de root (Nu se pot crea si trimite pachete syn altfel), daca nu, se intoarce mesajul ERROR.

Fisierul principal al scanner-ului este scanner.c.

Aici se considera variabilele ip si ports unde se vor extrage din argumentele date
ip-ul introdus si porturile de scanat.

Extract_IP.h defineste functia char * Extract_IP(char * option) din Extract_IP.c. In
aceasta functie se verifica daca ip-ul introdus este valid. De asemnea, se verifica daca
acesta este 127.0.0.1 (se considera ca nu se monitorizeaza masina pe care se executa
scanarea, deci aceasta este invalida ca target), 0.0.0.0 (tinta necunoscuta sau neaplicabila), 255.255.255.255 (local broadcast). In fiecare din aceste cazuri se semnaleaza eroare, alaturi
de cazurile cand adresa ip contine alte caractere in afara de cifre sau grupuri mai mari de 255
sau mai mici de 0. Daca ip-ul target trece toate aceste teste, este intors in functia scanner.c

Extract_Ports.h defineste functia char ** Extract_Ports(char * option, int * nr). Aici se
parcurge si se prelucreaza argumentul pentru porturi. Se verifica daca porturile sunt introduse
corespunzator si daca se incadreaza in pool-ul 1-65535. Functia intoarce un vector de string-uri
de tipul p1,p2,p3-p5..... In scanner.c se prelucreaza acest vector de stringuri rezultand un vector
de int-uri reprezentand porturile care urmeaza a fi scanate. (Ex: -p="21,22,79-82" => 
v = 21,22,79,80,81,82).

Send_Syn.h defineste functia int Start_It(int * ports, int nr, char * target) din Send_Syn.c.
Aceasta primeste ca parametri vectorul de porturi, lungimea acestuia si ip-ul tinta. Se afla ip-ul
host-ului pe care se lucreaza. Se verifica daca aceasta are conectivitate si apoi se declara 
structurile si socket-ul care urmeaza a fi folosite in scanare:
	struct sockaddr_in Destination;
	struct pseudo_header PSH;
	struct iphdr * IP_Header; -> header-ul ip
	struct tcphdr * TCP_Header; -> header-ul tcp
	int s = socket(AF_INET, SOCK_RAW, IPPROTO_TCP)
	
Se completeaza campurile header-elor cu informatiile necesare. Important este ca flag-ul syn
al header-ului tcp sa fie setat pe 1, aratandu-se astfel intentia de a initia conexiunea.
	
Inainte de a se incepe scanarea se lanseaza in executie un thread care asculta si proceseaza
datele primite:
	pthread_create( &sniffer_thread , NULL ,  start_sniffer , (void*) message1)
	
In cadrul unui for care parcurge vectorul de porturi se asigneaza campului corespunzator din
header-ul TCP valoarea portului tinta, se face check-sum-ul si se trimite pachetul.

Sniffer-ul mentionat mai sus asteapta pana cand OK = NR (unde OK este numarul de raspunsuri primite, iar NR este lungimea vectorului de porturi). In functia de sniffing (begin_sniffing),
daca un packet s-a primit cu succes, acesta se investigheaza (in functia analyze_data). Aici
este important sa se verifice campurile header-ului tcp. Daca flag-urile syn si ack sunt setate
pe 1, iar adresa ip sursa coincide cu ip-ul destinatie initial, inseamna ca portul este deschis.
In caz contrar, portul este inchis. La fiecare procesare, variabila OK este incrementata pentru a se
putea determina apoi daca s-a primit raspuns la toate pachetele syn sau nu.

Sunt implementate de asemenea si functionalitati pentru a verifica daca packet-ul primit este 
intr-adevar raspuns care corespunde unui port tinta (in caz contrar se ignora) si de oprire daca
dupa un anumit interval de timp nu se primeste nimic. Se seteaza un timp de start si un timp end.
Cand diferenta depaseste valoarea 2*NR (NR fiind numarul de porturi tinta), sniffing-ul se opreste.
Aceasta valoarea poate fi modificata. Se seteaza si valoarea de asteptare pentru un raspuns: 3 s.
Si aceasta poate fi modificata.

Obs: 1. Functiile pentru trimitere si sniffing de pachete, precum si cele de aflare si testare a 
ip-urilor sunt adaptarea si uniunea unor surse gasite pe internet si trimise de dvs. :)
	2. Uneori se intampla ca la prima testare, unele teste sa rezulte in FAILED, dar in PASSED la
rulari urmatoare.

