#ifndef __SENDDYN_H__
#define __SENDSYN_H__


extern char * My_IP();
extern unsigned short Check_Sum(unsigned short *ptr,int nbytes);
extern void analyze_data (unsigned char* buffer, int size);
extern void * begin_sniffing(void * ptr);
extern int Start_It(int * ports, int nr, char * target);
extern int Is_Here(int port_nr);

#endif/*__SENDSYN_H__*/
