#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<netinet/tcp.h>
#include<netinet/ip.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<pthread.h>
#include<errno.h>
#include<pthread.h>
#include<unistd.h>
#include<time.h>
#include "Send_Syn.h"

int OK = 0;
int NR;
time_t start_t, end_t;
double diff_t;
int *v;
struct pseudo_header
{
	
	unsigned int source_add;
	unsigned int dest_add;
	unsigned char placeholder;
	unsigned char protocol;
	unsigned short tcp_length;
	
	struct tcphdr TCP_H;
};


struct in_addr Dest_IP;

int Start_It(int * ports, int nr, char * target)
{
	int i;
	v = ports;
	NR = nr;
	//for(i = 0; i<nr; i++)
		//printf("Scanner %d: %d\n", i, ports[i]);
		
	char source_ip[32];
	char * sss = My_IP();

	strcpy(source_ip, sss);
	
	if(strcmp(source_ip, "127.0.0.1") == 0 || strcmp(source_ip, "0.0.0.0") == 0)
	{
		printf("ERROR\nNo connection.\n");
		exit(0);
	}
	//printf("My IP: %s\n", source_ip);

	char Datagram[4096]; //datagram
	memset(Datagram, 0, 4096);
	struct sockaddr_in Destination;
	struct pseudo_header PSH;
	struct iphdr * IP_Header = (struct iphdr *) Datagram; //headerul IP
	struct tcphdr * TCP_Header = (struct tcphdr *) (Datagram + sizeof(struct ip)); //headerul TCP
	
		
	
	int s = socket(AF_INET, SOCK_RAW, IPPROTO_TCP);
	
	if(s<0)
	{
		printf ("ERROR\nError creating socket\n");
        exit(0);
	}
	//else
	//	printf("Socket created!\n\n");

	Dest_IP.s_addr = inet_addr( target );

	
	//Se completeaza campurile din headerul IP
	IP_Header->version = 4;
	IP_Header->ihl = 5;
	IP_Header->tos = 0;
	IP_Header->tot_len = sizeof(struct ip) + sizeof(struct tcphdr);
	IP_Header->id = htons(54321);
	IP_Header->frag_off = htons(16384);
	IP_Header->ttl = 64;
	IP_Header->protocol = IPPROTO_TCP;
	IP_Header->check = 0;
	IP_Header->saddr = inet_addr(source_ip);
	IP_Header->daddr = Dest_IP.s_addr;
	
	IP_Header->check = Check_Sum((unsigned short *)Datagram, IP_Header->tot_len) >> 1;
	
	//Se completeaza campurile din headerul TCP
	TCP_Header->source = htons(1234);
	TCP_Header->dest = htons(80);
	TCP_Header->seq = htonl(1105024978);
	TCP_Header->ack_seq = 0;
	TCP_Header->doff = sizeof(struct tcphdr) / 4;
	TCP_Header->fin = 0;
	TCP_Header->syn = 1;
	TCP_Header->rst = 0;
	TCP_Header->psh = 0;
	TCP_Header->ack = 0;
	TCP_Header->urg = 0;
	TCP_Header->window = htons(14600);
	TCP_Header->check = 0;
	TCP_Header->urg_ptr = 0;
	

    int one = 1;
    const int *val = &one;
     
    if (setsockopt (s, IPPROTO_IP, IP_HDRINCL, val, sizeof (one)) < 0)
    {
        printf ("ERROR\nError setting IP_HDRINCL\n");
        exit(0);
    }
    
    //printf("Starting sniffer thread...\n");
    
    pthread_t thread_sniff;
 
    if( pthread_create( &thread_sniff , NULL ,  begin_sniffing , NULL) < 0)
    {
        printf ("ERROR\nCould not create the thread for sniffing\n");
        exit(0);
    }
 
     
    int port;
    Destination.sin_family = AF_INET;
    Destination.sin_addr.s_addr = Dest_IP.s_addr;
    char *p1, *p2, *q;
    for(port = 0 ; port < nr ; port++)
    {
    
		//printf("ports: %d\n", ports[port]);	
        TCP_Header->dest = htons ( ports[port]);
        TCP_Header->check = 0;
         
        PSH.source_add = inet_addr( source_ip );
        PSH.dest_add = Destination.sin_addr.s_addr;
        PSH.placeholder = 0;
        PSH.protocol = IPPROTO_TCP;
        PSH.tcp_length = htons( sizeof(struct tcphdr) );
         
        memcpy(&PSH.TCP_H , TCP_Header , sizeof (struct tcphdr));
         
        TCP_Header->check = Check_Sum( (unsigned short*) &PSH , sizeof (struct pseudo_header));
         
        //Send the packet
        sleep(1);
        if ( sendto (s, Datagram , sizeof(struct iphdr) + sizeof(struct tcphdr) , 0 , (struct sockaddr *) &Destination, sizeof (Destination)) < 0)
        {
            printf ("Error sending syn packet. Error number : %d . Error message : %s \n" , errno , strerror(errno));
            exit(0);
        }
    }
     
   pthread_join( thread_sniff , NULL);
        
	return 1;
 }
 
 unsigned short Check_Sum(unsigned short *ptr,int nbytes) 
{
    register long sum;
    unsigned short oddbyte;
    register short answer;
 
    sum=0;
    while(nbytes>1) {
        sum+=*ptr++;
        nbytes-=2;
    }
    if(nbytes==1) {
        oddbyte=0;
        *((u_char*)&oddbyte)=*(u_char*)ptr;
        sum+=oddbyte;
    }
 
    sum = (sum>>16)+(sum & 0xffff);
    sum = sum + (sum>>16);
    answer=(short)~sum;
     
    return(answer);
}



void analyze_data(unsigned char* buffer, int size)
{
    struct iphdr *iph = (struct iphdr*)buffer;
    struct sockaddr_in source,dest;
    unsigned short iphdrlen;
     
    if(iph->protocol == 6)
    {
        struct iphdr *iph = (struct iphdr *)buffer;
        iphdrlen = iph->ihl*4;
     
        struct tcphdr *tcph=(struct tcphdr*)(buffer + iphdrlen);
             
        memset(&source, 0, sizeof(source));
        memset(&dest, 0, sizeof(dest));
        
        source.sin_addr.s_addr = iph->saddr;
        dest.sin_addr.s_addr = iph->daddr;
         
        if(Is_Here(ntohs(tcph->source)))
        {
		    if(tcph->syn == 1 && tcph->ack == 1 && source.sin_addr.s_addr == Dest_IP.s_addr )
		    {
		        printf("%d - OPEN\n" , ntohs(tcph->source));
				fflush(stdout);
		        OK++;
		    }
		    else
		    {
		    	printf("%d - CLOSED\n" , ntohs(tcph->source));
				fflush(stdout);
		        OK++;
		    }
        }
      
    }
}
 
void * begin_sniffing(void * pointer)
{  
	struct sockaddr sock_addr;
    int sock_addr_size = sizeof sock_addr;
    int data_size;
     
    unsigned char *data = (unsigned char *)malloc(65536);
    int sock = socket(AF_INET , SOCK_RAW , IPPROTO_TCP);
     
    if(sock < 0)
    {
        printf("ERROR\nSocket error\n");
        return;
    }
     
    struct timeval tv;
    tv.tv_sec = 3;
    tv.tv_usec = 0;
    setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval));
    time(&start_t);
    diff_t = 0;
    //printf("Max: %f\n", (double)NR * 2);
    while(OK < NR && diff_t < (double)NR * 2)
    {
//    	printf("1\n");
        data_size = recvfrom(sock , data , 65536 , 0 , &sock_addr , &sock_addr_size);
  //       printf("2\n");
        if(data_size <0 )
        {
            printf("ERROR\nPacket !received\n");
            return;
        }
         
        analyze_data(data , data_size);
        time(&end_t);
        diff_t = difftime(end_t, start_t);
        start_t = end_t;
      // printf("Diff: %f\n", diff_t);
    }
     
    close(sock);

}
 
 
 char * My_IP()
{
	char * buf = (char *)malloc(20);
	char * ip = (char *)malloc(20);

	FILE *ph = popen("sudo ifconfig | grep 'inet addr' | cut -d':' -f2 | cut -d' ' -f1 | head -1", "r");
	while (fgets(buf, sizeof buf, ph)) {
	   // printf("%s", buf);
		strcat(ip, buf);
	}
	pclose(ph);
	strcpy(ip + strlen(ip)-1, ip + strlen(ip));
	//printf("Din gasire:%s----%d", ip, strlen(ip));
	 return ip;
}

int Is_Here(int port_nr)
{
	int i, ok = 0;
	for(i=0; i<NR; i++)
		if(v[i] == port_nr)
		{
			ok = 1;
			break;
		}
		
	return ok;
	
}
	
