#include<stdio.h>
#include<stdlib.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include "Extract_IP.h"
#include "Extract_Ports.h"
#include "Send_Syn.h"

char * ip;
char ** ports;

void main(int argc, char **argv)
{
	int i, j = 0, k, nr = 0;
	int *v = NULL;

	if(argc != 3)
		printf("ERROR\nNumar insuficient de parametri");
	else
	{

		ip = Extract_IP(argv[1]);
		
		//printf("IP: %s\n", ip);
		ports = Extract_Ports(argv[2], &nr);		
		
		//printf("Scanner: Nr ports: %d\n", nr);
		//for(i = 0; i<nr; i++)
			//printf("%s, ", ports[i]);
		
		char *p1, *p2, *q;
		for(i=0; i<nr; i++)
		{
			
			q = strchr(ports[i], '-');
			
			if( q != NULL)
			{
				p1 = (char *)malloc(q - ports[i] + 1);
				p2 = (char *)malloc(ports[i] + strlen(ports[i]) - q);
				strncpy(p1, ports[i], q-ports[i]);
				strncpy(p2, q+1, ports[i] + strlen(ports[i]) - q );
			 	
			 	//printf("\n\n%s si %s\n", p1, p2);
			 	int nr_p1 = atoi(p1);
			 	int nr_p2 = atoi(p2);
			 	if(nr_p1 > nr_p2)
			 	{
			 		printf("ERROR\nInvalid range, ports are descending, they must be ascending\n");
			 		exit(0);
			 	}
			 	for(k = nr_p1; k<=nr_p2; k++)
			 	{
			 		j++;
			 		//printf("j: %d\n", j);
			 		v = (int *)realloc(v, j * sizeof(int));
			 		v[j-1] = k;
			 		
			 	}
			 		 	
			}
			else
			{
				//printf("fara\n");
				j++;
				//printf("j: %d\n", j);
				v = (int *)realloc(v, j * sizeof(int));
				v[j-1] = atoi(ports[i]);

			}
		}
		
		//for(i = 0; i<j; i++)
			//printf("%d, ", v[i]);
		Start_It(v, j, ip);
		
	
	}
}
