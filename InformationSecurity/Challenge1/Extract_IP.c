#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<arpa/inet.h>
#include "Extract_IP.h"

void Check_IP(char * ip)
{
	struct sockaddr_in sa;
	char *IpAdd = (char *)malloc(20);
	strcpy(IpAdd, ip);
	
	int result = inet_pton(AF_INET, IpAdd, &(sa.sin_addr));
	
	if(result == 1)
	{
		if(strcmp(IpAdd, "127.0.0.1") == 0)
		{
			printf("ERROR\nNo network connection\n");
			exit(0);
		}
		else
			if(strcmp(IpAdd, "255.255.255.255") == 0)
			{
				printf("ERROR\nTarget is local broadcast\n");
				exit(0);
			}
		else
				if(strcmp(IpAdd, "0.0.0.0") == 0)
		{
			printf("ERROR\nNot a valid target ip\n");
			exit(0);
		}
	}
	else
	{
		printf("ERROR\nInvalid ip address\n");
		exit(0);
	}
}

char * Extract_IP(char * option)
{
	//printf("Prelucram %s\n", option);
	
	//se verifica sintaxa comenzii
	if(option[0] != '-' || option[1] != 'i' || 
	   option[2] != 'p' || option[3] != '=')
	{
		printf("ERROR\nCommand usage: -ip=\"ip_addrs\"\n");
		exit(0);
	}
	
	if(option[strlen(option)-1] == '=')
	{
		printf("ERROR\nMissing IP\n");
		exit(0);
	}
	//printf("ok\n");
	
	char * p = strchr(option, '=');	
	int length = option + strlen(option) - p;
	char * ip = (char *)malloc(sizeof(char) * length);
	strncpy(ip, p+1, option + strlen(option) - p);
	ip[length] = '\0';
	//printf("ip:%s\n", ip);
	Check_IP(ip);
	
	return ip;
	}
