#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "Extract_Ports.h"

char ** ports;

int Check_Range(char * sir)
{
	int n = atoi(sir);
	//printf("n: %d\n", n);
	if(n<0 || n>=65535)
	{
		printf("ERROR\nPort out of range [1..65535]\n");
		exit(0);
	}
	
	return 1;
}

void Check_Block2(char * block, int * nr)
{
	char sir[12], *p, *q;
	int ok = 0;
	//printf("block: %s\n", block);
	strcpy(sir, "0123456789-"); sir[11] = '\0';
	if(block[0] == '0')
	{
		printf("ERROR\n in block 1\nInvalid port number\n");
		exit(0);
	}
	
	int i;
	for(i=0; i<strlen(block); i++)
	{	//printf("caut %c\n", *(block+i));

			
		if(strchr(sir, *(block+i)) == NULL)
		{
			printf("ERROR\n in block 3\nInvalid port number\n");
			exit(0);
		}
		
		else 
		 if(*(block + i) == '-')
			if(strchr( block+i+1, '-') != NULL)
			{
				printf("ERROR\n in block 2\nInvalid port number\n");
				exit(0);
			}
			else
			
				if(strchr(sir, *(block + i + 1)) == NULL || *(block + i + 1)=='\0')
				{
					printf("ERROR\nMissing port\n");
					exit(0);
				}
				else
				{
					ok = 1;
					q = block + i ;
				}
	}
	
	if(ok)
	{
		char *p1 = (char *)malloc(q - block + 1);
		strncpy(p1, block, q-block);
		char *p2 = (char *)malloc(block + strlen(block) - q);;
		strncpy(p2, q+1, block + strlen(block) - q);
		//printf("p1:%s\np2:%s\n", p1, p2);
		Check_Range(p1);
		Check_Range(p2);
		ok = 0;
	}
	else
		Check_Range(block);
	
	
	//printf("NR: %d\n", *nr);
	ports = (char **)realloc(ports, *nr * sizeof (char *));
	ports[*nr - 1] = (char *)malloc(strlen(block));
	strcpy(ports[*nr-1], block);
	

}

void Check_Ports(char * ports1, int * nr)
{
	//printf("We have: %s\n", ports);
	//strcat(ports, ",");
	
	char *p, *q;
	p = strtok(ports1, ",");
	while (p)
	{
		*nr = *nr + 1;
		Check_Block2(p,nr);
		//printf("nr: %d, %s\n", *nr, p);
		p=strtok(NULL, ",");
		
	}
	
	//printf("Final nr: %d\n", *nr);
}

char ** Extract_Ports(char * option, int * nr)
{
	//printf("Prelucram %s\n", option);

		//se verifica sintaxa comenzii
	if(option[0] != '-' || option[1] != 'p' || option[2] != '=')
	{
		printf("ERROR\nCommand usage: -port=\"port1,port2-portn\"\n");
		exit(0);
	}
	
	if(option[strlen(option)-1] == '=')
	{
		printf("ERROR\nMissing port(s)\n");
		exit(0);
	}
	//printf("ok\n");
	
	char * p = strchr(option, '=');	
	int length = option + strlen(option) - p;
	char * ports1 = (char *)malloc(sizeof(char) * length);
	strncpy(ports1, p+1, option + strlen(option) - p);
	ports1[length] = '\0';
	//printf("Ports:%s\n", ports);
	
	Check_Ports(ports1, nr);
	int i;
	
	/*
	printf("Dupa :\n");
		for(i = 0; i<*nr; i++)
			printf("%s, ", ports[i]);
	*/
	return ports;
	
}
