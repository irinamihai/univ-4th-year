#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/utsname.h>
#include <openssl/bn.h>
#include <openssl/aes.h>
#include "Transform.h"
#include "AesOperations.h"

int NR = 0;


void main(int argc, char ** argv)
{
	if(argc != 3)
	{
		printf("ERROR\nWrong call of function!");
		exit(0);
	}
	
	int operation = atoi(argv[1]);
	if(operation < 1 || operation > 5)
	{
		printf("ERROR\nNo such server.");
		exit(0);
	}
	
	int port = atoi(argv[2]);
	if(port <= 0 || port > 65535)
	{
		printf("ERROR\nWrong port number");
		exit(0);
	}
	
	char * secret_key = (char *)malloc(16);
	struct sockaddr_in server;
	struct sockaddr_in client;
	int readSock, connSock;
	
	readSock=socket(AF_INET, SOCK_STREAM,0);
	bzero(&server, sizeof(server));
	server.sin_family=AF_INET;
	server.sin_addr.s_addr=htonl(INADDR_ANY);
	server.sin_port=htons(port);
	bind(readSock,(struct sockaddr*) &server, sizeof(server));
	listen(readSock,10);
	
	while(1)
	{
		int length=sizeof(client);
		connSock=accept(readSock, (struct sockaddr*)&client,&length);
	
		if(!fork())
		{
			close(readSock);
			char receiveBuff[4096], sendBuff[4096];
			memset(sendBuff, 0, 128);
			memset(receiveBuff, 0, 128);
	
			//----------Diffie Hellman----------			
			//se primeste de la gateway p
			char *p=(char *)malloc(sizeof(char)*16);
			int status = recv(connSock, p, 16,0);
				
			//printf("P:%s\n", p);
			BIGNUM * P_BN = BN_new();		
			BN_bin2bn(p, 16, P_BN);
			
			//se formeaza si se trimite la gateway g
			char * g = (char *)malloc(16);
			memset(g,0,16);
			sprintf(g, "%d", 5);
			BIGNUM * G_BN = BN_new();
			BN_bin2bn((unsigned char *)g, 16, G_BN);
			send(connSock, g, 16, 0);

			//se primeste cheia publica a gateway-ului
			char * gatebuf = (char *)malloc(16);
			recv(connSock, gatebuf, 16,0);
			BIGNUM * A_BN = BN_new();
			BN_bin2bn((unsigned char *)gatebuf, 16, A_BN);
								
			//se calc cheia publica B a serverului si se trim la gateway
			BIGNUM * b_BN = BN_new(); //nr secret ales de server
			BN_rand(b_BN, 128, 0, 0);
			BIGNUM * B_BN = BN_new(); //pt cheia publica a serverului
			BN_CTX *ctx = BN_CTX_new();
			BN_mod_exp(B_BN, G_BN, b_BN, P_BN, ctx);
			char * send2 = (char *)malloc(16);
			BN_bn2bin(B_BN, (unsigned char *)send2);
			send(connSock,send2,16,0);
					
			//se calculeaza cheia secreta
			BIGNUM * SECRET = BN_new();
			BN_mod_exp(SECRET, A_BN, b_BN, P_BN, ctx);
			BN_bn2bin(SECRET, (unsigned char *) secret_key);
			//printf("Secret:%s\n", BN_bn2dec(SECRET));	
			
			do
			{
 				status = recv(connSock, &receiveBuff, 128, 0);
				
				char * decrypted_string;
				decrypt_string(receiveBuff, &decrypted_string, secret_key);
				Depad_String(&decrypted_string);
				
				char * processedString = TransformString(decrypted_string, operation);
				
				//encrypt the processedString
				char *encrypted_string;
				encrypt_string(processedString, &encrypted_string, secret_key);
				
				//send it to the gateway
				send(connSock,encrypted_string,128,0);
				
			}while(status != 0);

			close (connSock);
			exit(0);
		}
		else
			close(connSock);
	}
	close(readSock);
}
