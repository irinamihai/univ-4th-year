package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"encoding/json"
	"flag"
	"strconv"
)

type Message struct {
    Opcode string
    Request string
}

var ip string
var port string
var opcode string
var request string

func initParameters() {
	portOk := func(port int) bool {			
		if port <= 0 || port > 65535 {
			return false
		}
		return true
	}
	opcodeOk := func(opcode int) bool {
		if opcode < 1 || opcode > 5 {
			return false
		}
		return true
	}
	//load the parameters
	flag.StringVar(&ip, "ip", "", "gateway ip")
	flag.StringVar(&port, "port", "", "gateway port")
	flag.StringVar(&opcode, "opcode", "", "required opcode; values in range [1..5]")
	flag.StringVar(&request, "request", "", "required request")
	flag.Parse()
//check the parameters
	ErrorHandler(len(ip) == 0, "Missing IP!")	
	tmpIp := net.ParseIP(ip)
	ErrorHandler(tmpIp == nil, "Invalid IP!")
	
	ErrorHandler(len(port) == 0, "Missing port/port interval!")
	tmpPort, err := strconv.Atoi(port)
	ErrorHandler(err != nil, "Invalid port format detected")
	ErrorHandler(!portOk(int(tmpPort)), "Port out of range [1..65535]")
	
	ErrorHandler(len(opcode) == 0, "Missing opcode")
	tmpOpcode, err := strconv.Atoi(opcode)
	ErrorHandler(err != nil, "Invalid opcode format detected")
	ErrorHandler(!opcodeOk(int(tmpOpcode)), "Opcode out of range [1..5]")

	ErrorHandler(len(request) == 0, "Missing request")
}

//go run client.go -ip="127.0.0.1" -port="3540" -opcode="2" -request="aab"
func main() {	
	initParameters()

	conn, err := net.Dial("tcp", ip + ":" + port)
	ErrorHandler(err!=nil, "Error on connect")	
		
	obj, err := json.Marshal(Message{opcode, request})
	ErrorHandler(err!=nil, "Error on creating JSON object")
	
	fmt.Fprintf(conn, string(obj)+"\n")
	
	b := bufio.NewReader(conn)
	line, err := b.ReadBytes('\n')
	ErrorHandler(err!=nil, "Error on reading from gateway")
	
	fmt.Println(string(line))
}

func ErrorHandler(throwError bool, errMessage string) {
	if throwError == true {
		fmt.Println("ERROR")
		fmt.Println(errMessage)
		os.Exit(0)
	}
}
