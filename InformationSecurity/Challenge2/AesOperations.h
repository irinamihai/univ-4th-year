#ifndef __AESOPERATIONS_H__
#define __AESOPERATIONS_H__

extern void encrypt_string(char * Message, char ** result, char * KeyChar);
extern void decrypt_string(char * Message, char ** result, char * KeyChar);
extern void Depad_String(char ** theString);

#endif/*__AESOPERATIONS_H__*/