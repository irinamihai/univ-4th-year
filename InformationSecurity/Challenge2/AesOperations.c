#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <openssl/aes.h>
#include "AesOperations.h"

void encrypt_string(char * Message, char ** result, char * KeyChar)
{
	// se face padding
	int Len = strlen(Message);
	int Pad = 0;
	
	if(Len%16 != 0)
		Pad = 16 - Len%16;
	
	char *msg = (char *)malloc(Len + Pad);
	strcpy(msg, Message);
	memset(msg + Len, '0', Pad);
	msg[Len + Pad] = '\0';
	//se stabileste iv-ul
	char iv[16];
	memset(iv, 1, 16);
	
	//se cripteaza
	AES_KEY * key = (AES_KEY *)malloc(sizeof(AES_KEY) + 1);
	
	*result = (char *)malloc(strlen(msg) + 1);

	AES_set_encrypt_key((unsigned char *)KeyChar, 128, key);
	
	AES_cbc_encrypt((unsigned char *)msg, (unsigned char *)*result, (unsigned long)strlen(msg), key, (unsigned char *)iv, AES_ENCRYPT);
	(*result)[strlen(msg)] = '\0';
}


void decrypt_string(char * Message, char ** result, char * KeyChar)
{
	int Len = strlen(Message);

	//se stabileste iv-ul
	char iv[16];
	memset(iv, 1, 16);
	
	//se decripteaza
	*result = (char *)malloc(strlen(Message) + 1);
	AES_KEY * key = (AES_KEY *)malloc(sizeof(AES_KEY) + 1);
	
	AES_set_decrypt_key((unsigned char *)KeyChar, 128, key);
	
	AES_cbc_encrypt((unsigned char *)Message, (unsigned char *)*result, (unsigned long)strlen(Message), key, (unsigned char *)iv, AES_DECRYPT);

	(*result)[strlen(Message)] = '\0';
}

void Depad_String(char ** theString)
{
	int ok, nr;
	ok = 1;
	
	while(ok)
	{
		nr = strlen(*theString) - 1;
		
		if((*theString)[nr] == '0')
			(*theString)[nr] = '\0';
		else
			ok = 0;
	}
	
}
