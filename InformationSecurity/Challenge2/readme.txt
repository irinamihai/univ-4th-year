Tema 2

---Gateway-ul---
Fisierul gateway.c are implementate functionalitatile necesare functionarii 
gateway-ului din cerinta. La inceput este declarata o structura serverInfo in care
se vor retine informatiile despre servere si operatia aferenta. Apoi se verifica 
numarul de parametri si portul pe care asculta gateway-ul. Urmeaza apoi citirea 
din fisierul de configurare. Aici, pe masura ce se citesc date, se realoca memorie 
in vectorul Servers de tip serverInfo. Se verifica si integritatea datelor citite
(ip, port, operatie), intorcandu-se eroare daca datele nu corespund. La sfarsitul 
acestei fct vom sti cate servere avem, si vector vom avea ip-urile acestora, 
porturile pe care ele asculta si operatiile pe care fiecare server in parte le poate 
efectua.

Se creeaza apoi socket-ul connSock folosit la comunicarea cu clientul. Dupa primirea 
unor date de la client, prima oara se parseaza sirul (aflat in format Json) pentru 
a determina operatia ceruta si sirul care se cere a fi transformat. Se cauta apoi 
in vectorul de Servere informatiile serverului care efectueaza operatia ceruta.
Daca nu exista server care sa se ocupe de operatia respectiva, se intoarce eroare. 
Aici nu mai are sens verificarea operatiei, intrucat aceasta verificare se face in 
client.

Incepe apoi protocolul Diffie-Hellman de stabilire a cheii secrete pe 128 de biti. 
Aici se trimit mesaje succesive intre gateway si server. Comentariile din cod 
sunt mai mult decat elocvente pentru descierea operatiilor. Cheia secreta se salveaza 
in variabila secret_key. Apoi se cripteaza sirul primit cu AES CBC folosindu-se cheia 
secreta, se trimite la server si se asteapta. Odata raspunsul primit, acesta se 
decripteaza, se trece in format Json si se trimite la client.

---Server-ul---
Fisierul server.c are implementate functionalitatile necesare functionarii serverului 
din cerinta temei. Prima oara se verifica numarul de parametri, apoi operatia si 
portul. Se creeaza socketul folosit la comunicarea cu gateway-ul, aferent portului 
pe care serverul asculta. Incepe apoi protocolul Diffie Hellman pentru determinarea 
cheii secrete salvate in variabila secret_key. Dupa primirea sirului care de doreste 
a fi transformat, se decripteaza, se face depadding, si se proceseaza. Rezultatul se 
cripteaza si se trimite la gateway. In fisierul Transform.c se afla operatiile de 
lucru cu sirurile primite. Functiile din acesta sunt declarate in header-ul 
Transform.h inclus atat in Transform.c, cat si in server.c

Observatii:
	- pentru criptare si lucrul cu Big Numbers (folosite in Diffie Hellman) s-a instalat 
biblioteca openssl in /usr/include
	- in fisierul AesOperations.c se afla functiile pentru criptarea si decriptarea cu 
AES, modul CBC, precum si functia de depadding. Acestea sunt declarate in header-ul 
AesOperations.h inclus atat in AesOperations.c, cat si in gateway.c si server.c.
	- se mai intampla uneori ca sirurile sa nu fie integral trimise, rezultand astfel 
raspunsuri eronate ajunse la client. Acest lucru se intampla datorita fragmentarii, 
ordinii pachetele, cat de ocupata este reteaua. Se poate observa insa ca nu este o 
problema pentru una dintre operatii. Rulari succesive ale checker-ului arata ca 
fiecare test este trecut.
