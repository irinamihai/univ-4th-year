#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/utsname.h>
#include <openssl/bn.h>
#include <openssl/aes.h>
#include "AesOperations.h"

int readSock, connSock;
int NR = 0;
int linesNumber;

typedef struct serverInfo
{
	char serverIP[16];
	int port;
	int operation;
	
}serverInfo;

serverInfo ** Servers = NULL; 
int linesNumber = 0;

void ParseJson(char * s, char **op, char **sir);
void ReadFromFile(char * inputFile, int port);
void CreateJson(char (* sendBuff)[], char * decrypted_string, char * op);
void GetServer(char * op, int * serverPort, char **serverIP);
void Check_IP(char * ip);

void main(int argc, char ** argv)
{
	if(argc != 3)
	{
		printf("ERROR\nWrong call of function");
		exit(0);
	}
	
	char * inputFile = argv[1];
	int port = atoi(argv[2]);
	if(port <= 0 || port > 65535)
	{
		printf("ERROR\nWrong port number");
		exit(0);
	}
	
	ReadFromFile(inputFile, port);
	
	int i;
	char * secret_key = (char *)malloc(16);
	struct sockaddr_in server;
	struct sockaddr_in client;
	
	readSock=socket(AF_INET, SOCK_STREAM,0);
	bzero(&server, sizeof(server));
	server.sin_family=AF_INET;
	server.sin_addr.s_addr=htonl(INADDR_ANY);
	server.sin_port=htons(port);
	bind(readSock,(struct sockaddr*) &server, sizeof(server));
	listen(readSock,10);
	
	while(1)
	{
		int length=sizeof(client);
		connSock=accept(readSock, (struct sockaddr*)&client,&length);
	
		if(!fork())
		{
			close(readSock);
			char receiveBuff[4096],sendBuff[4096];
			int sockX, serverPort;
			int status = recv(connSock, &receiveBuff, 128,0);
			char *op, *sir, *serverIp;
			
			ParseJson(receiveBuff, &op, &sir);	
			GetServer(op, &serverPort, &serverIp);
			
			struct sockaddr_in serverX;
			sockX=socket(AF_INET,SOCK_STREAM,0);
			bzero(&serverX,sizeof(serverX));
			serverX.sin_family=AF_INET;
			inet_pton(AF_INET,serverIp, &serverX.sin_addr);
			serverX.sin_port=htons(serverPort);
			int statusX=connect(sockX,(struct sockaddr*) &serverX,	sizeof(serverX)); 
	
			//se incepe protocolul Diffie-Hellman pentru stabilirea cheii secrete
			//se obtine p
			BIGNUM * P_BN = BN_new();
			BIGNUM * G_BN = BN_new();
			BIGNUM * A_BN = BN_new();
			BIGNUM * a_BN = BN_new();
			BIGNUM * B_BN = BN_new();
			BN_rand(a_BN, 128, 0, 0);//nr secret ales de gateway
		
			//se genereaza p - prim si se trimite la sever
			BN_generate_prime_ex(P_BN, 128,0, NULL,NULL,NULL);
			char * P = (char *)malloc(16);
			memset(P,0,16);
			BN_bn2bin(P_BN, (unsigned char *)P);	
			send(sockX, P, 16, 0);
				
			//se primeste g
			char * g = (char *)malloc(16);
			recv(sockX, g, 16,0);
			BN_bin2bn((unsigned char *)g, 16, G_BN);
			
			//se calc cheia publica A a gateway-ului si se trim la server
			BN_CTX *ctx = BN_CTX_new();
			BN_mod_exp(A_BN, G_BN, a_BN, P_BN, ctx);
			char *send1 = (char *)malloc(16);
			BN_bn2bin(A_BN, (unsigned char *)send1);
			send(sockX,send1,16,0);
					
			//se primeste cheia publica a serverului
			char *serverbuf = (char *)malloc(16);
			recv(sockX, serverbuf, 16,0);
			BN_bin2bn((unsigned char *)serverbuf, 16, B_BN);						
			
			//se calc cheia secreta
			BIGNUM * SECRET = BN_new();
			BN_mod_exp(SECRET, B_BN, a_BN, P_BN, ctx);
			//printf("Secret:%s\n", BN_bn2dec(SECRET));
		
			BN_bn2bin(SECRET, (unsigned char *)secret_key);

			while(status!=0)
			{				
				char * encrypted_string;
				encrypt_string(sir, &encrypted_string, secret_key);

				send(sockX,encrypted_string,128,0);
				
				//primeste raspunsul final de la server
				status = recv(sockX, &receiveBuff, 128,0);
			
				//decripteaza receiveBuff
				char * decrypted_string;
				decrypt_string(receiveBuff, &decrypted_string, secret_key);
				Depad_String(&decrypted_string);
			
				//se trece raspunsul in format Json
				CreateJson(&sendBuff, decrypted_string, op);
				
				//se trimite la client raspunsul
				send(connSock, &sendBuff, 128,0);
			}
			
			shutdown(sockX,SHUT_RDWR);
			close(sockX);
			close(connSock);
			close(readSock);
	
			exit(0);
		}
		else
			close(connSock);
	}
	close(readSock);
}


void ParseJson(char * s, char **op, char **sir)
{
	char *p, *q, *r;
		
	*op = (char *)malloc(2*sizeof(char));
	
	int i = 0;
	int nr1, nr2;
	
	p = strchr(s, '\"');
	
	for(i = 0; i < 7; i++)
	{
		q = strchr(p+1, '\"');
		
		if(i == 1)
		{
			(*op)[0] = *(q+1);
			(*op)[1] = '\0';
		}
		
		if(i == 5)
			nr1 = q - s + 1;
		
		if(i == 6)
		{
			nr2 = q - s;
			*sir = (char *)malloc(nr2 - nr1 + 1);
			memcpy(*sir, s + nr1, nr2 - nr1);
			(*sir)[nr2-nr1] = '\0';
		}
		
		p = q + 1;
	}
}

//citeste linie cu linie si numara-le
void ReadFromFile(char * inputFile, int port)
{
	FILE *fp = fopen(inputFile, "r");
	
	if(fp == NULL)
	{
		printf("ERROR\nNo config file for Gateway\n");
		exit(0);
	}
	
	char *line = NULL;
    size_t len = 0;
    ssize_t read;
		   
	int i;

    if (fp == NULL)
		exit(EXIT_FAILURE);

    while ((read = getline(&line, &len, fp)) != -1)
	{
	   linesNumber ++;
		
		serverInfo ** temp = (serverInfo **)realloc(Servers, linesNumber * sizeof(serverInfo *));
			
		if(temp == NULL)
		{	
			printf("ERROR\nRealloc malfunctioned\n");
			exit(0);
		}
		Servers = temp;
		Servers[linesNumber-1] = (serverInfo *)malloc(sizeof(serverInfo));
		sscanf(line, "%s %d %d", Servers[linesNumber-1]->serverIP, &Servers[linesNumber-1]->port, &Servers[linesNumber-1]->operation);
		
		//se verifica ip-ul
		Check_IP(Servers[linesNumber-1]->serverIP);
		
		//se verifica portul
		if(Servers[linesNumber-1]->port <= 0 || Servers[linesNumber-1]->port > 65535)
		{
			printf("ERROR\nWrong port number\n");
			exit(0);
		}
		
		//se verifica operatia
		if(Servers[linesNumber-1]->operation > 5 || Servers[linesNumber-1]->operation < 1)
		{
			printf("ERROR\nWrong operation code: %d\n", Servers[linesNumber-1]->operation);
			exit(0);
		}
    }

    free(line);	   
	fclose(fp);			   
}

void CreateJson(char (* sendBuff)[], char * decrypted_string, char * op)
{
	char sir1[11];
	
	memcpy(sir1, "{\"Opcode\":\"", 11);
	memcpy(*sendBuff, sir1, strlen(sir1));
	memcpy(*sendBuff + 11, op, 1);
 	memcpy(*sendBuff + 12, "\",\"Response\":\"", 14);
	memcpy(*sendBuff + 26, decrypted_string, strlen(decrypted_string));
	memcpy(*sendBuff + 26 + strlen(decrypted_string), "\"}\n", 3);
	
	(*sendBuff)[29 + strlen(decrypted_string)] = '\0';	
}

//foloseste la determinare datelor despre serverul care efectueaza operatia ceruta
void GetServer(char * op, int * serverPort, char **serverIP)
{
	int i;
	
	for(i=0; i<linesNumber; i++)
	{
		if(Servers[i]->operation == atoi(op))
		{
			*serverPort = Servers[i]->port;
			*serverIP = (char *)malloc(16);
			strcpy(*serverIP, Servers[i]->serverIP);
			strcat(*serverIP, "\0");
			return;
		}
	}
	
	printf("ERROR\nOperation not found: %d\n", atoi(op));
}

void Check_IP(char * ip)
{
	struct sockaddr_in sa;
	char *IpAdd = (char *)malloc(20);
	strcpy(IpAdd, ip);
	
	int result = inet_pton(AF_INET, IpAdd, &(sa.sin_addr));
	
	if(result == 1)
	{
		if(strcmp(IpAdd, "255.255.255.255") == 0)
		{
				printf("ERROR\nTarget is local broadcast\n");
				exit(0);
		}
		else
			if(strcmp(IpAdd, "0.0.0.0") == 0)
			{
				printf("ERROR\nNot a valid target ip\n");
				exit(0);
			}	
	}
	else
	{
		printf("ERROR\nInvalid ip address: %s\n", ip);
		exit(0);
	}
}
