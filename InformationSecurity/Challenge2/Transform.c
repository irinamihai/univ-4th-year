#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include "Transform.h"

void function1(char *input, char **output);
void function2(char *input, char **output);
void function3(char *input, char **output);
void function4(char *input, char **output);
void function5(char *input, char **output);

char * TransformString(char * inputString, int nr)
{
	char *output;
	
	switch(nr)
	{
		case 1:	function1(inputString, &output);
				return output;
		
		case 2: function2(inputString, &output);
				return output;
		
		case 3: function3(inputString, &output);
				return output;
				
		case 4: function4(inputString, &output);
				return output;
				
		case 5: function5(inputString, &output);
				return output;					
	}
}

void function1(char *input, char **output)
{
	*output = (char *)malloc(strlen(input) + 1);
	
	char *sPtr = input;
	while(*sPtr != '\0')
      {	
  		if(islower(*sPtr))
              *sPtr = toupper(*sPtr);
              
        *sPtr++;
       }
       
    strcpy(*output, input);
}

void function2(char *input, char **output)
{
	*output = (char *)malloc(strlen(input) + 1);
	
	char *sPtr = input;
	while(*sPtr != '\0')
      {
  		if(isupper(*sPtr))
        *sPtr = tolower(*sPtr);
              
        *sPtr++;
       }
       
    strcpy(*output, input);
}

void function3(char *input, char **output)
{
	*output = (char *)malloc(strlen(input) + 1);
	
	char *p = input;
	while(p)
	{
		int nr = 0;
		while(*p == ' ' &&  *p!='\0')
			p++;
		
		if(*p == '\0')
			break;
			
		*p = toupper(*p);
		p++;

		while(*p != ' ' && *p != '\0')
		{
			*p = tolower(*p);
			p++;
		}
		if(*p == '\0')
			break;
	}

    strcpy(*output, input);
}

void function4(char *input, char **output)
{
	int i,j, nr = 0;
	char *p;

	function3(input, &input);
	
	for(j = 0; j<strlen(input); j++)
		if(input[j] == ' ')
			nr++;
			
	int length = strlen(input) - nr;
	*output = (char *)malloc(length + 2);
	p = *output;
	j=0;
	
	for(i=0; i<strlen(input); i++)
	{
		if(input[i] != ' ')
		{
			p[j] = input[i];
			j++;
		}
	}
	
	(*output)[length] = '\0';
}

void function5(char *input, char **output)
{
	int i,j, nr = 0;
	char *p;
	
	for(j = 0; j<strlen(input); j++)
		if(input[j] == ' ')
			nr++;
			
	int length = strlen(input) - nr;
	*output = (char *)malloc(length + 2);
	p = *output;
	j=0;
	
	for(i=0; i<strlen(input); i++)
	{
		if(input[i] != ' ')
		{
			p[j] = input[i];
			j++;
		}
	}
	
	(*output)[length] = '\0';
}
