#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

void writeToFile(char *magicNumber, int lines, int columns, int maxValue, int **image, char *fileName);
void getNextImage(int **localImage, int stripSize, int columns, int ***newLocalImage, int d, int rank);
int getChange(int lines, int columns, int **I, int ** newI);
void copyNewOverLocal(int lines, int columns, int ***localImage, int ***newLocalImage);
void initializeLocalMatrix(int rank, int size, int stripSize, int columns, int ***localImage, int ***newLocalImage, int **image);
void heartBeat(int task, int rank, int size, int columns, int stripSize, int ***localImage);
void getPredictors(int rank, int columns, int stripSize, int **I, int ***newLocalImage, double a, double b, double c);
void getRezidualMatrix(int rank, int columns, int lines, int ***localImage, int **newLocalImage);
int getValuePosition(int *values, int value, int showSize);
void applyFilter(int size, int stripSize, int columns, int **localImage, int ***newLocalImage);
void writeInFinalMatrix(int rank, int size, int stripSize, int columns, int ***taskImage, int **newLocalImage);
void getDifferent(int rank, int size, int *different);
void workLine(int *showSize, int **values, int **appear, int *v, int columns);

int main(int argc, char ** argv)
{

  MPI_Status status;
  int rank, //rangul unui procesor 
	  size, //numarul de procesoare
	  lines, columns, //numarul de linii si de colonae ale imaginii
	  maxValue, //valoarea maxima din reprezentarea imaginii
	  stripSize, //cate linii se vor aloca fiecarui procesor
	  localStripSize,
	  matrixPosition,
	  i, j, k, //contoare
	  different,
	  d, temp;
  int **image=NULL, **task1NewImage=NULL, **localImage=NULL, **newLocalImage=NULL, **rezImage=NULL;
	double a, b, c;
	char *p;
  
  char magicNumber[3];
  FILE *f;
  
  MPI_Init (&argc, &argv); //MPI starts
  MPI_Comm_rank(MPI_COMM_WORLD, &rank); //get the process id
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
  //procesul cu rangul 0 va citi din fisier
  if(rank == 0)
  {
		
		if(argc != 6)
		{
			printf("Apel incorect al functiei\n");
			MPI_Abort(MPI_COMM_WORLD, 7777);
		}

		a = strtod(argv[5], &p);
		b = strtod(argv[4], &p);
		c = strtod(argv[3], &p);
		d = atoi(argv[2]);
		//printf("d:%d\n", d);
	//	printf("Eu sunt %d\n", rank);
	//	printf("File: %s\n", argv[1]);
		f = fopen(argv[1], "r");

		if(f == NULL)
		{
			printf("Nu exista acest fisier!\n");
			MPI_Abort(MPI_COMM_WORLD, 7777);
		}
		//se citesc primele 3 linii din fisier
		int ok = fscanf(f, "%s %d %d %d", magicNumber, &columns, &lines, &maxValue);

		//printf("lines %d columns %d size %d\n", lines, columns, size);

		if(!ok)
		{
			printf("Nu s-a putut citi din fisier!\n");
			MPI_Abort(MPI_COMM_WORLD, 7777);
		}

		image = (int **)malloc(lines * sizeof(int *));
		task1NewImage = (int **)malloc(lines * sizeof(int *));
		
		for(i=0; i<lines; i++)
		{
			image[i] = (int *)malloc(columns * sizeof(int));
			task1NewImage[i] = (int *)malloc(columns * sizeof(int));
		}

		for(i=0; i<lines; i++)
		{
			for(j=0; j<columns; j++)
			{
				ok = fscanf(f, "%d", &image[i][j]);

				if(!ok)
				{
					printf("Nu s-a putut citi din fisier!\n");
					MPI_Abort(MPI_COMM_WORLD, 7777);
				}
				//printf("%d  ", image[i][j]);
			}
			//printf("\n");
		}
		//printf("\n\n");
		fclose(f);		
  }

  //trimite la celelalte procesoare lines, columns
  MPI_Bcast(&lines, 1, MPI_INT, 0, MPI_COMM_WORLD);//linii
  MPI_Bcast(&columns, 1, MPI_INT, 0, MPI_COMM_WORLD);//coloane
	MPI_Bcast(&d, 1, MPI_INT, 0, MPI_COMM_WORLD);//coloane
	MPI_Bcast(&a, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Bcast(&b, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	MPI_Bcast(&c, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	//se calculeaza stripsize
	stripSize = lines / size;
	//printf("strip: %d\n", stripSize);
	if(rank == size-1 && lines%size != 0)
	{
		stripSize = lines - stripSize*(size-1);
		//printf("Last one stripsize: %d\n", stripSize);
	}
  
  MPI_Barrier(MPI_COMM_WORLD);

  //alocare  matrici locale, apoi 0 trimite si celelalte primesc
	//se face doar la taskul1, apoi se refolosesc la celelalte
  for(k=0; k<size; k++)
  {
		if(rank == k)
		{
			localImage = (int **)malloc((stripSize + 2) * sizeof(int *));
			newLocalImage = (int **)malloc((stripSize + 2) * sizeof(int *));
			rezImage = (int **)malloc((stripSize + 2) * sizeof(int *));
			
			if(localImage == NULL || newLocalImage == NULL || rezImage == NULL)
			{
				printf("Procesul %d nu a mai putut aloca memorie pentru intreaga matrice!\n", k);
				MPI_Abort(MPI_COMM_WORLD, 7777);
			}

			for(i=0; i<stripSize+2; i++)
			{
				localImage[i] = (int *)malloc(columns * sizeof(int));
				newLocalImage[i] = (int *)malloc(columns * sizeof(int));
				rezImage[i] = (int *)malloc(columns * sizeof(int));
				
				if(localImage[i] == NULL || newLocalImage[i] == NULL)
				{
					printf("Procesul %d nu a mai putut aloca memorie pentru linii!\n", k);
					MPI_Abort(MPI_COMM_WORLD, 7777);
				}
			}
		}
  }
  
  //initializarea matricilor locale
	initializeLocalMatrix(rank, size, stripSize,columns, &localImage, &newLocalImage, image);

	for (i=0; i<columns; i++)
	{
		localImage[0][i] = 0;
		localImage[stripSize+1][i] = 0;
		newLocalImage[0][i] = 0;
		newLocalImage[stripSize+1][i] = 0;
	}
	
	MPI_Barrier(MPI_COMM_WORLD);

	while(different)
	{
			MPI_Barrier(MPI_COMM_WORLD);
			heartBeat(1,rank, size, columns, stripSize, &localImage);
			MPI_Barrier(MPI_COMM_WORLD);

			getNextImage(localImage, stripSize+2, columns, &newLocalImage, d, rank);
			different = getChange(stripSize+2, columns, localImage, newLocalImage);

			//if-else in fct separata cu param:rank, size, different (pointer)
			if(rank == 0)
			{
				for(k=1; k<size; k++)
				{
					MPI_Recv(&temp, 1, MPI_INT, k, 1, MPI_COMM_WORLD, &status);
					different = different || temp;
				}
				
				for(k=1; k<size; k++)
					MPI_Send(&different, 1, MPI_INT, k, 1, MPI_COMM_WORLD);
			}
			else
			{
				MPI_Send(&different, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
				MPI_Recv(&different, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
			}
			copyNewOverLocal(stripSize+2, columns, &localImage, &newLocalImage);
	}

	//se trec rezultatele in matricea finala
	writeInFinalMatrix(rank, size, stripSize, columns, &task1NewImage, localImage);
	
	MPI_Barrier(MPI_COMM_WORLD);

  if(rank == 0)
		writeToFile(magicNumber, lines, columns, maxValue, task1NewImage, "img_task1.pgm");

	MPI_Barrier(MPI_COMM_WORLD);

	//begin task2
	
	initializeLocalMatrix(rank, size, stripSize,columns, &localImage, &newLocalImage, image);
	applyFilter(size, stripSize+2, columns, localImage, &newLocalImage);
	
	//se trec rezultatele in matricea finala
	writeInFinalMatrix(rank, size, stripSize, columns, &task1NewImage, newLocalImage);
	MPI_Barrier(MPI_COMM_WORLD);
	
	if(rank == 0)
		writeToFile(magicNumber, lines, columns, maxValue, task1NewImage, "img_task2.pgm");
	
	MPI_Barrier(MPI_COMM_WORLD);

	
	//begin task 3
	initializeLocalMatrix(rank, size, stripSize,columns, &localImage, &rezImage, image);

	MPI_Barrier(MPI_COMM_WORLD);
	heartBeat(3, rank, size, columns, stripSize, &localImage);
	MPI_Barrier(MPI_COMM_WORLD);
	
	getPredictors(rank, columns, stripSize+2, localImage, &rezImage, a, b, c);
	getRezidualMatrix(rank, columns, stripSize+2, &rezImage, localImage);

	if(rank == 0)
	{
		int *v=NULL, *appear=NULL, *values=NULL;
		int showSize = 0;

		//aici se vor primi date de la celelalte procese
		v = (int *)malloc(columns * sizeof(int));

		for(i=1; i<stripSize+1; i++)
			workLine(&showSize, &values, &appear, rezImage[i], columns);

				//se primeste stripsize-ul fiecarui procesor, apoi fiecare linie a lui
				matrixPosition = stripSize;
				
				for (k=1; k<size; k++)
				{
					MPI_Recv(&localStripSize, 1, MPI_INT, k, 1, MPI_COMM_WORLD, &status); // receive stripSize from k

					for(i=0; i<localStripSize; i++)
					{
						MPI_Recv(v, columns, MPI_INT, k, 1, MPI_COMM_WORLD, &status);
						workLine(&showSize, &values, &appear, v, columns);
					}
					matrixPosition += localStripSize;
				}

			double entropy = 0.0;
			int total = lines*columns;
			for(j=0; j<showSize; j++)
			{
				printf("%d %d\n", appear[j], values[j]);
				double frecv = (double)appear[j]/(double)total;
				entropy -= frecv * log2(frecv);
			}

			FILE *g = fopen("file_task3.txt", "w");
			fprintf(g, "%lf\n", entropy);
			fclose(g);
	}
	else
	{
		//fiecare procesor isi trimite stripsize-ul si apoi pe rand liniile
		MPI_Send(&stripSize, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
		for (i=0; i<stripSize; i++)
			MPI_Send(rezImage[i+1], columns, MPI_INT, 0, 1, MPI_COMM_WORLD);
	}
	
	MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
 
  return 0;
}

void workLine(int *showSize, int **values, int **appear, int *v, int columns)
{
	int j;
	//prelucrare linie primita
	for(j=0; j<columns; j++)
	{
		int position = getValuePosition(*values, v[j], *showSize);
		if(position >= 0)
			(*appear)[position]++;
		else
		{
			(*showSize)++;
			*values = (int *)realloc(*values, *showSize * sizeof(int));
			*appear = (int *)realloc(*appear, *showSize * sizeof(int));

			(*values)[*showSize-1] = v[j];
			(*appear)[*showSize-1] = 1;
		}
	}
}

void writeInFinalMatrix(int rank, int size, int stripSize, int columns, int ***taskImage, int **newLocalImage)
{
	int i, j, k, matrixPosition, localStripSize;
	MPI_Status status;
	
	if (rank == 0)
	{
		for (i=0; i<stripSize; i++)
			for (j=0; j<columns; j++)
				(*taskImage)[i][j] = newLocalImage[i+1][j];
			
		matrixPosition = stripSize;
		
		for (k=1; k<size; k++)
		{
			MPI_Recv(&localStripSize, 1, MPI_INT, k, 1, MPI_COMM_WORLD, &status); // receive stripSize from k
			for (i=0; i<localStripSize; i++)
				MPI_Recv((*taskImage)[matrixPosition+i], columns, MPI_INT, k, 1, MPI_COMM_WORLD, &status);
			
			matrixPosition += localStripSize;
		}
	}
	else
	{
		MPI_Send(&stripSize, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
		for (i=0; i<stripSize; i++)
			MPI_Send(newLocalImage[i+1], columns, MPI_INT, 0, 1, MPI_COMM_WORLD);
	}
}

void applyFilter(int size, int stripSize, int columns, int **L, int ***I)
{
	int i, j;
	for(i=1; i<stripSize-1; i++)
	{
		for(j=0; j<columns; j++)
			if(j == 0)
				(*I)[i][j] = (2*L[i-1][j] + L[i-1][j+1] + 4*L[i][j] + 2*L[i][j+1] + 2*L[i+1][j] + L[i+1][j+1])/16;
			else
				if(j == columns-1)
					(*I)[i][j] = (L[i-1][j-1] + 2*L[i-1][j] + 2*L[i][j-1] + 4*L[i][j]  + L[i+1][j-1] + 2*L[i+1][j])/16;
				else
					(*I)[i][j] = (L[i-1][j-1] + 2*L[i-1][j] + L[i-1][j+1] + 2*L[i][j-1] + 4*L[i][j] + 2*L[i][j+1] + L[i+1][j-1] + 2*L[i+1][j] + L[i+1][j+1])/16;
		}
}

int getValuePosition(int *values, int value, int showSize)
{
	int i;
	for(i=0; i<showSize; i++)
	{
		if(values[i] == value)
			return i;
	}

	return -1;
}

void getRezidualMatrix(int rank, int columns, int lines, int ***rezImage, int **localImage)
{
	int i, j;
	for(i=1; i<lines-1; i++)
		for(j=0; j<columns; j++)
			(*rezImage)[i][j] = localImage[i][j] - (*rezImage)[i][j];
}

void getPredictors(int rank, int columns, int lines, int **I, int ***newLocalImage, double a, double b, double c)
{
	int i, j;

	for(i=1; i<lines-1; i++)
	{
		for(j=0; j<columns; j++)
		{
			if(j == 0)
				(*newLocalImage)[i][j] = a*I[i-1][j];
			else
				(*newLocalImage)[i][j] = a*I[i-1][j] + b*I[i-1][j-1] + c*I[i][j-1];
		}
	}
}

void heartBeat(int task, int rank, int size, int columns, int stripSize, int ***localImage)
{
	MPI_Status status;

	if (rank % 2)
	{
		if (rank != 0)//trimit si primesc (de) sus
		{
			MPI_Send((*localImage)[1], columns, MPI_INT, rank-1, 1, MPI_COMM_WORLD);
			MPI_Recv((*localImage)[0], columns, MPI_INT, rank-1, 1, MPI_COMM_WORLD, &status);
		}
		
		if (rank != size-1) //trimit si primesc (de) jos
		{
			MPI_Send((*localImage)[stripSize], columns, MPI_INT, rank+1, 1, MPI_COMM_WORLD);
			MPI_Recv((*localImage)[stripSize+1], columns, MPI_INT, rank+1, 1, MPI_COMM_WORLD, &status);
		}
	}
	else
	{
		if (rank != size-1)//primesc si trimit (de) jos
		{
			MPI_Recv((*localImage)[stripSize+1], columns, MPI_INT, rank+1, 1, MPI_COMM_WORLD, &status);
			MPI_Send((*localImage)[stripSize], columns, MPI_INT, rank+1, 1, MPI_COMM_WORLD);
		}
		
		if (rank != 0)//primesc si trimit (de) sus
		{
			MPI_Recv((*localImage)[0], columns, MPI_INT, rank-1, 1, MPI_COMM_WORLD, &status);
			MPI_Send((*localImage)[1], columns, MPI_INT, rank-1, 1, MPI_COMM_WORLD);
		}
	}
}

void initializeLocalMatrix(int rank, int size, int stripSize, int columns, int ***localImage, int ***newLocalImage, int **image)
{
	MPI_Status status;
	int matrixPosition, localStripSize, i, j, k;
	
	if(rank == 0)
	{
		//initializare matrice locala pentru procesul 0
		for(i=0; i<stripSize; i++)
			for(j=0; j<columns; j++)
			{
				(*localImage)[i+1][j] = image[i][j];
				(*newLocalImage)[i+1][j] = image[i][j];
			}
		//se primeste stripSize-ul fiecarui procesor si apoi se trimit datele pentru ele
		matrixPosition = stripSize;
		for(k=1; k<size; k++)
		{
			MPI_Recv(&localStripSize, 1, MPI_INT, k, 1, MPI_COMM_WORLD, &status);
			
			for(i=0; i<localStripSize; i++)
				MPI_Send(image[matrixPosition+i], columns, MPI_INT, k, 1, MPI_COMM_WORLD);
			
			matrixPosition += localStripSize;
		}
	}
	else
	{
		MPI_Send(&stripSize, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
		
		for(i=0; i<stripSize; i++)
			MPI_Recv((*localImage)[i+1], columns, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
	}
}

void getNextImage(int **I, int lines, int columns, int ***newImage, int d, int rank)
{
	int i, j, k,l, alightNeighbours;

	for(i=1; i<lines-1; i++)
	{
		for(j=0; j<columns; j++)
		{
			alightNeighbours = 0;
			
			for(k = i - 1; k <= i + 1; k++)
			{
				for(l = j - 1; l <= j + 1; l++)
				{
					if(k==i && l==j)
						continue;
					
					if(l<0 || l>columns-1)
						continue;

					if(I[k][l])
						alightNeighbours++;					
				}
			}

				if(alightNeighbours < d)
					(*newImage)[i][j] = 0;
				else
					(*newImage)[i][j] = I[i][j];		
		}
	}
}

int getChange(int lines, int columns, int **I, int ** newI)
{
	int i, j;
	for(i=1; i<lines-1; i++)
		for(j=0; j<columns; j++)
			if(I[i][j] != newI[i][j])
				return 1;

	return 0;
}

void copyNewOverLocal(int lines, int columns, int ***localImage, int ***newLocalImage)
{
	int i, j;

	for(i=1; i<lines-1; i++)
		for(j=0; j<columns; j++)
			(*localImage)[i][j] = (*newLocalImage)[i][j];
}

void writeToFile(char *magicNumber, int lines, int columns, int maxValue, int **image, char *fileName)
{
	FILE *final = fopen(fileName, "w");
  int i, j;
  
  fprintf(final, "%s\n%d %d\n%d\n", magicNumber, columns, lines, maxValue);
  
  for(i=0; i<lines; i++)
		for(j=0; j<columns; j++)
			fprintf(final, "%d\n", image[i][j]);
}

