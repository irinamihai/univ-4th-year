#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

void Get_Generation(char *** Now_Generation, int Lines, int Columns, int chunk, int Gen);
int Get_Alive_Neighbours(char ** Matrix, int m, int n, int i, int j);
char Get_Cell_Next_State(int Alive_Neighbours, char Now_State);
void Write_To_File(char ** Now_Generation, int Lines, int Columns, char * );
void Free_Memory(char ** Now_Generation, int Lines, int Columns);


int main(int argc, char ** argv)
{
	if(argc != 4)
	{
		printf("ERROR!\nNu exista suficiente argumente");
		exit(0);
	}
		
	//matricea corespunzatoare generatiei initiale
	char **Now_Generation;
	int i, j, k, File_Position, nthreads, tid, Lines, Columns, chunk, Last_Lines, m, n, Generations;
	FILE *f;
	
	Generations = atoi(argv[1]);
	
	//se citeste din fisier
	#pragma omp parallel shared(Now_Generation, File_Position, chunk, Lines, Columns) private(nthreads, tid, i, j, f, Last_Lines)
	{
		tid = omp_get_thread_num();
		nthreads = omp_get_num_threads();
		
		if(tid == 0)
		{	
			f = fopen(argv[2], "r");
			
			if(f == NULL)
			{
				printf("ERROR opening file\n");
				exit(0);
			}
			
			fscanf(f, "%d", &Lines);
			fscanf(f, "%d", &Columns);
			
			if(Lines < 3 || Columns  < 3)
			{
			  FILE *g = fopen(argv[3], "w");
			  fprintf(g, "%s", "configuratie invalida\n");
			  fclose(g);
			  exit(0);
			}

			File_Position = ftell(f);			
			chunk = Lines/nthreads;
			Now_Generation = (char **)malloc(Lines * sizeof(char *));
			
			for(k = 0; k<Lines; k++)
			  Now_Generation[k] = (char *)malloc(Columns*sizeof(char));
		}

		#pragma omp barrier
		if (tid!=0)
		{
		  f = fopen(argv[2], "r");
		  fseek(f, tid * chunk * Columns * 2 + File_Position + 1, SEEK_SET);
		}
		else
		  fseek(f, File_Position + 1, SEEK_SET);
		  
		if(tid == nthreads - 1)
		  Last_Lines = Lines - chunk * nthreads;
		else
			Last_Lines = 0;
		
		for(i=tid*chunk; i<(tid+1)*chunk + Last_Lines; i++)
		{
		  char c2;
		
		  for(j = 0; j < Columns; j++)
		  {
				fscanf(f, "%c", &Now_Generation[i][j]);
				fscanf(f, "%c", &c2);	
		  }
		}
		
			fclose(f);
	}
	
	//se calculeaza generatia dorita
	Get_Generation(&Now_Generation, Lines, Columns, chunk, Generations);
	//se scriu datele in fisier
	Write_To_File(Now_Generation, Lines, Columns, argv[3]);
	//se elibereaza memoria
	Free_Memory(Now_Generation, Lines, Columns);
	
	return 0;
}

void Get_Generation(char *** Now_Generation, int Lines, int Columns, int chunk, int Gen)
{
  int i, j, k, nthreads, Last_Lines, tid;
  char ** Next_Generation;
  
  Next_Generation = (char **)malloc(Lines * sizeof(char *));
  
  for(i = 0; i<Lines; i++)
		Next_Generation[i] = (char *)malloc(Columns*sizeof(char));	
  
  for(k=0; k<Gen; k++)
  {
  	#pragma omp parallel shared(Now_Generation, Next_Generation, chunk, Lines, Columns) private(nthreads, tid, i, j, Last_Lines)
  	{
			tid = omp_get_thread_num();
			nthreads = omp_get_num_threads();
	
			if(tid == nthreads - 1)
				Last_Lines = Lines - chunk * nthreads;
			else
				Last_Lines = 0;
	
			for(i = tid*chunk; i < (tid+1) * chunk + Last_Lines; i++)
				for(j = 0; j < Columns; j++)
				{
					int Alive_Neighbours = Get_Alive_Neighbours(*Now_Generation, Lines, Columns, i, j);
					int Cell_State = Get_Cell_Next_State(Alive_Neighbours, (*Now_Generation)[i][j]);
		
					Next_Generation[i][j] = Cell_State;
				}
				
			#pragma omp barrier
			for(i = tid*chunk; i < (tid+1) * chunk + Last_Lines; i++)
				for(j = 0; j < Columns; j++)
						(*Now_Generation)[i][j] = Next_Generation[i][j];
		}
  } 
}


int Get_Alive_Neighbours(char ** Matrix, int m, int n, int i, int j)
{
  int k, l;
  int count = 0;
  
  //daca este prima linie
  if(i==0)
	//daca este elementul din stanga sus
	if(j==0)
	{
	  count = count + Matrix[m-1][n-1] + Matrix[m-1][0] + Matrix[m-1][1] + Matrix[0][1] + Matrix[1][1] + Matrix[1][0] + Matrix[1][n-1] + Matrix[0][n-1];
	  return count - 384;
	}
	else
	  if (j==n-1)
	  {
		  //daca este elementul din dreapta sus
			count = count + Matrix[m-1][n-2] + Matrix[m-1][n-1] + Matrix[m-1][0] + Matrix[0][0] + Matrix[1][0] + Matrix[1][n-1] + Matrix[1][n-2] + Matrix[0][n-2];
			return count - 384;
	  }
	  else
	  {
			//daca este orice alt element de pe prima linie
			count = count + Matrix[m-1][j-1] + Matrix[m-1][j] + Matrix[m-1][j+1] + Matrix[0][j+1] + Matrix[1][j+1] + Matrix[1][j] + Matrix[1][j-1] + Matrix[0][j-1];
			return count - 384;
	  }
	    
	  //daca este ultima linie
	  if(i==m-1)
		//daca este elementul din stanga jos
		if(j==0)
		{
		  count = count + Matrix[m-2][n-1] + Matrix[m-2][0] + Matrix[m-2][1] + Matrix[m-1][1] + Matrix[0][1] + Matrix[0][0] + Matrix[0][n-1] + Matrix[m-1][n-1];
		  return count - 384;
		}
		else
		  if (j==n-1)
		  {
		  	//daca este elementul din dreapta jos
				count = count + Matrix[m-2][n-2] + Matrix[m-2][n-1] + Matrix[m-2][0] + Matrix[m-1][0] + Matrix[0][0] + Matrix[0][n-1] + Matrix[0][n-2] + Matrix[m-1][n-2];
				return count - 384;
		  }
		  else
		  {
				//daca este orice alt element de pe ultima linie
				count = count + Matrix[m-2][j-1] + Matrix[m-2][j] + Matrix[m-2][j+1] + Matrix[m-1][j+1] + Matrix[0][j+1] + Matrix[0][j] + Matrix[0][j-1] + Matrix[m-1][j-1];
				return count - 384;
		  }
		 
		  //daca este orice alt element de pe prima coloana
		  if(j==0)
		  {
				count = count + Matrix[i-1][n-1] + Matrix[i-1][0] + Matrix[i-1][1] + Matrix[i][1] + Matrix[i+1][1] + Matrix[i+1][0] + Matrix[i+1][n-1] + Matrix[i][n-1];
				return count - 384;
		  }
		  if(j==n-1)
		  {
				//daca este orice alt element de pe ultima coloana
				count = count + Matrix[i-1][n-2] + Matrix[i-1][n-1] + Matrix[i-1][0] + Matrix[i][0] + Matrix[i+1][0] + Matrix[i+1][n-1] + Matrix[i+1][n-2] + Matrix[i][n-2];
				return count - 384;
		  }
		    
		  for(k = i - 1; k <= i + 1; k++)
				for(l = j - 1; l <= j + 1; l++)
				{
				  if(k==i && l==j)
					continue;
			  
				  count += Matrix[k][l];
				}
			
			return count - 384;
}

char Get_Cell_Next_State(int alive, char Now_State)
{
  char Next_State;
  
  if(Now_State == '0')
		if(alive == 3)
	  	return '1';
		else
	  	return '0';
  
  if(Now_State == '1')
  {
		if(alive == 0 || alive == 1)
	  	return '0';
	
		if(alive >=4 )
	  	return '0';
	
		if(alive == 2 || alive == 3)
	  	return '1';
  }
}

void Write_To_File(char ** Now_Generation, int Lines, int Columns, char * fileName)
{
	int i, j;
	
	FILE *f = fopen(fileName, "w");
	
	for(i=0; i<Lines; i++)
	{
	  for(j=0; j<Columns; j++)
	  {
			fprintf(f, "%c", Now_Generation[i][j]);
			fprintf(f, "%c", ' ');
	  }
	  
	  fprintf(f, "%s", "\n");
	}
	
	fclose(f);
}

void Free_Memory(char ** Now_Generation, int Lines, int Columns)
{
	int i,j;
	
	for(i=0; i<Lines; i++)
			free(Now_Generation[i]);
		
	free(Now_Generation);
}

