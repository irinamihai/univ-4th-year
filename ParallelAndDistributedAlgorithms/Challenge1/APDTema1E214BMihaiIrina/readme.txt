APD - Tema 1 - Mihai Irina, E214B

Rezolvarea problemei GoL este impartita in doua etape:
	- citirea in paralel a datelor din fisierul dat ca parametru
	- jocul propriu-zis
	
Citirea din fisier a datelor:
	- fiecare thread calculeaza tid si numarul de thread-uri
	- thread-ul master deschide fisierul si citeste nr de linii si nr de coloane
		si face alocarea pentru matricea corespunzatoare generatiei initiale; de
		asemenea, daca nr de linii sau de coloane este < 3, jocul se incheie.
	- thread-ul master retine pozitia in fisier in variabila shared File_Position
	- celelalte thread-uri deschid fisierul si se pozitioneaza corespunzator
		tid-ului: fiecare thread va citi un numar de linii egal cu chunk,
		iar ultimul thread va citi si restul liniilor. Pentru acest lucru se
		foloseste variabila Last_Lines reprezentand nr de linii in + pe care fiecare
		thread trebuie sa le citeasca. Ea are valoarea 0 pentru toate thread-urile,
		mai putin pentru ultimul thread.
	- fiecare thread intra intr-un for unde citeste datele din fisier si le pune
		in matricea Now_Generation.
		
Jocul:
	- functia Get_Generation determina generatia cautata.
	- se aloca o matrice de char-uri de aceleasi dimensiuni ca matricea initiala.
	- se intra intr-un for care merge de la 0 la numarul generatiei care se doreste.
	- aici se intra intr-o regiune paralela unde fiecare thread analizeaza o 
		anumita portiune din matricea curenta. Se determina pentru fiecare celula 
		cati vecini are (in fct Alive_Neighbours) si in fct de acestia se determina	
		starea ei in generatia urmatoare si se inscrie in matricea Next_Generation
	- se pune o bariera pentru ca apoi sa se realizeze copierea datelor in matricea 
		curenta a matricii corespunzatoare generatiei urmatoare.

La sfarsitul fct main, se scrie in fisierul de iesire configuratia generatiei 
dorite si se elibereaza datele alocate.
